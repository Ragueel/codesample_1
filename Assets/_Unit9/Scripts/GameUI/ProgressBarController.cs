﻿// Created by Ragueel

using UnityEngine;
using UnityEngine.UI;

namespace _Unit9.Scripts.GameUI
{
    public class ProgressBarController : MonoBehaviour
    {
        [SerializeField] private Image _barForeground;

        /// <summary>
        /// Sets the value of progress bar
        /// </summary>
        /// <param name="val">Should be between 0f-1f</param>
        public void SetValue(float val)
        {
            _barForeground.fillAmount = Mathf.Clamp(val, 0f, 1f);
        }
    }
}