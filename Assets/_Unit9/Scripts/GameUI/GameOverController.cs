﻿// Created by Ragueel

using _Unit9.Scripts.GameSO;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Unit9.Scripts.GameUI
{
    public class GameOverController : MonoBehaviour
    {
        [SerializeField] private Transform _player;
        [SerializeField] private Transform _movePosition;
        [SerializeField] private GameEventSO _onPlayerDeath;

        private void OnEnable()
        {
            _onPlayerDeath.AddListener(MovePlayer);
        }

        private void OnDisable()
        {
            _onPlayerDeath.RemoveListener(MovePlayer);
        }

        private void MovePlayer()
        {
            _player.position = _movePosition.position;
        }

        public void Restart()
        {
            SceneManager.LoadScene(0);
        }
    }
}