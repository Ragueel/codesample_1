﻿// Created by Ragueel

using _Unit9.Scripts.GameSO;
using UnityEngine;
using UnityEngine.UI;

namespace _Unit9.Scripts.GameUI
{
    public class GameplayUI : MonoBehaviour
    {
        [Header("Gameplay Data")] [SerializeField]
        private FloatLiveData _playerHp;

        [SerializeField] private FloatLiveData _playerScore;
        [SerializeField] private FloatFieldSO _playerMaxHp;


        [Header("UI Controls")] [SerializeField]
        private ProgressBarController _playerHpBar;

        [SerializeField] private Text _scoreText;

        private void OnEnable()
        {
            _playerHp.AddListener(OnPlayerHpChange);
            _playerScore.AddListener(OnPlayerScoreChange);
        }

        private void OnDisable()
        {
            _playerHp.RemoveListener(OnPlayerHpChange);
            _playerScore.RemoveListener(OnPlayerScoreChange);
        }

        private void OnPlayerScoreChange()
        {
            _scoreText.text = $"Score: {_playerScore.Value}";
        }

        private void OnPlayerHpChange()
        {
            _playerHpBar.SetValue(_playerHp.Value / _playerMaxHp);
        }
    }
}