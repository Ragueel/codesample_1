﻿// Created by Ragueel

using _Unit9.Scripts.GameSO;
using UnityEngine;
using UnityEngine.UI;

namespace _Unit9.Scripts.GameUI
{
    public class WaveTextController : MonoBehaviour
    {
        [SerializeField] private GameEventSO _onWaveCompleted;
        [SerializeField] private Animator _animator;
        [SerializeField] private Text _text;
        private int _currentWave = 1;
        private static readonly int ShowTrigger = Animator.StringToHash("_Show");

        private void OnEnable()
        {
            _onWaveCompleted.AddListener(OnWaveComplete);
        }

        private void OnDisable()
        {
            _onWaveCompleted.RemoveListener(OnWaveComplete);
        }

        private void OnWaveComplete()
        {
            _animator.SetTrigger(ShowTrigger);
            _text.text = $"Wave {_currentWave} Completed";
            _currentWave++;
        }
    }
}