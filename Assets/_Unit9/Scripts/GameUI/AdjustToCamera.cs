﻿// Created by Ragueel

using UnityEngine;

namespace _Unit9.Scripts.GameUI
{
    /// <summary>
    /// UI was not working correctly on the phone, so I changed ui to world space
    /// and made something like a Screen Space - Camera
    /// </summary>
    public class AdjustToCamera : MonoBehaviour
    {
        [SerializeField] private float _distance = 3;
        [SerializeField] private Vector3 _offset;
        private Camera _camera;

        private void Awake()
        {
            _camera = Camera.main;
        }

        private void LateUpdate()
        {
            transform.position = _offset + _camera.transform.forward * _distance;
        }
    }
}