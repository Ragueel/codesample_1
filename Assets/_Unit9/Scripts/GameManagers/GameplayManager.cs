﻿// Created by Ragueel

using _Unit9.Scripts.Enemies;
using _Unit9.Scripts.GameSO;
using UnityEngine;

namespace _Unit9.Scripts.GameManagers
{
    [DefaultExecutionOrder(-100)]
    public class GameplayManager : MonoBehaviour
    {
        [SerializeField] private FloatLiveData _playerScore;
        [SerializeField] private EnemiesSpawner _enemiesSpawner;
        [SerializeField] private GameEventSO _onScoreUpdate;
        [SerializeField] private GameEventSO _onEnemyDeath;
        public static GameplayManager Instance;

        private void Awake()
        {
            Instance = this;
        }

        private void OnEnable()
        {
            _onEnemyDeath.AddListener(OnEnemyDeath);
        }

        private void OnDisable()
        {
            _onEnemyDeath.RemoveListener(OnEnemyDeath);
        }

        private void OnEnemyDeath()
        {
            _playerScore.Value += 15;
            _onScoreUpdate.Raise();
        }

        private void Start()
        {
            StartGame();
        }

        private void StartGame()
        {
            _playerScore.Value = 0;

            _enemiesSpawner.SpawnEnemies();
        }
    }
}