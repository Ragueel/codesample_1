﻿// Created by Ragueel

using _Unit9.Scripts.GameSO;
using UnityEngine;

namespace _Unit9.Scripts.GameManagers
{
    public class PoolsInitializer : MonoBehaviour
    {
        [SerializeField] private SimpleObjectsPoolSO[] _pools;

        private void Start()
        {
            foreach (SimpleObjectsPoolSO simpleObjectsPoolSo in _pools)
            {
                simpleObjectsPoolSo.TryToInitialize();
            }
        }

        private void OnDestroy()
        {
            foreach (SimpleObjectsPoolSO simpleObjectsPoolSo in _pools)
            {
                simpleObjectsPoolSo.Clear();
            }
        }
    }
}