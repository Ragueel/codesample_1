﻿// Created by Ragueel

using _Unit9.Scripts.Weapons;
using UnityEngine;

namespace _Unit9.Scripts.GamePlayer
{
    public class PlayerShootController : MonoBehaviour
    {
        [SerializeField] private BaseWeapon _weapon;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
            {
                _weapon.Shoot();
            }
        }
    }
}