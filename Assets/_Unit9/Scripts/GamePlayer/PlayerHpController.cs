﻿// Created by Ragueel

using _Unit9.Scripts.GameCore;
using _Unit9.Scripts.GameSO;
using UnityEngine;

namespace _Unit9.Scripts.GamePlayer
{
    public class PlayerHpController : MonoBehaviour, IDamageable, IKillable
    {
        [SerializeField] private FloatFieldSO _maxHp;
        [SerializeField] private FloatLiveData _currentHp;
        [SerializeField] private GameEventSO _onPlayerDeath;

        private void Start()
        {
            _currentHp.Value = _maxHp;
        }

        public void TakeDamage(float amount)
        {
            _currentHp.Value -= amount;

            if (_currentHp.Value <= 0)
            {
                Die();
            }
        }

        public void Die()
        {
            _onPlayerDeath.Raise();
        }
    }
}