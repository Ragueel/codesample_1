﻿// Created by Ragueel

using _Unit9.Scripts.GameCore;
using _Unit9.Scripts.GamePlayer;
using UnityEngine;

namespace _Unit9.Scripts.Weapons.Bullets
{
    public class EnemyBullet : BaseBullet
    {
        protected override void HandleCollision(Collision other)
        {
            if (other.collider.CompareTag("Player"))
            {
                other.collider.gameObject.GetComponent<IDamageable>()?.TakeDamage(_damageAmount.Value);
            }

            Die();
        }
    }
}