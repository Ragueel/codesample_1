﻿// Created by Ragueel

using _Unit9.Scripts.GameCore;
using _Unit9.Scripts.GamePlayer;
using UnityEngine;

namespace _Unit9.Scripts.Weapons.Bullets
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerBullet : BaseBullet
    {
        
        protected override void HandleCollision(Collision other)
        {
            if (other.collider.CompareTag("Enemy"))
            {
                other.collider.gameObject.GetComponent<IDamageable>()?.TakeDamage(_damageAmount.Value);
            }

            Die();
        }
    }
}