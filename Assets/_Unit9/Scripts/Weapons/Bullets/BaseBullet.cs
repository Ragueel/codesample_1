﻿// Created by Ragueel

using System.Collections;
using _Unit9.Scripts.GameCore;
using _Unit9.Scripts.GameSO;
using UnityEngine;

namespace _Unit9.Scripts.Weapons.Bullets
{
    [RequireComponent(typeof(Rigidbody))]
    public abstract class BaseBullet : MonoBehaviour, IKillable
    {
        [SerializeField] protected FloatFieldSO _damageAmount;
        [SerializeField] protected FloatFieldSO _movementSpeed;
        [SerializeField] protected SimpleObjectsPoolSO _hitEffectPool;
        [SerializeField] protected SimpleObjectsPoolSO _bulletPool;
        [SerializeField] protected TrailRenderer _trailRenderer;

        protected Rigidbody _rigidbody;

        const float EffectLength = 1f;


        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _rigidbody.useGravity = false;
        }

        private void OnCollisionEnter(Collision other)
        {
            HandleCollision(other);
        }

        protected abstract void HandleCollision(Collision other);

        public void SetDirection(Vector3 shootDirection)
        {
            _rigidbody.angularVelocity = Vector3.zero;
            _rigidbody.velocity = shootDirection * _movementSpeed;
            StartCoroutine(AutoDeathRoutine());
        }

        private IEnumerator AutoDeathRoutine()
        {
            yield return new WaitForSeconds(5f);
            Die();
        }

        public void Die()
        {
            var effect = _hitEffectPool.GetElementWithPositionRotation(transform.position, transform.rotation);
            _trailRenderer.Clear();
            _hitEffectPool.PutElementDelayed(effect, EffectLength);
            _bulletPool.PutElement(gameObject);
        }
    }
}