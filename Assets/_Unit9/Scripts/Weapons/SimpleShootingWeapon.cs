﻿// Created by Ragueel

using _Unit9.Scripts.Weapons.Bullets;
using UnityEngine;

namespace _Unit9.Scripts.Weapons
{
    public class SimpleShootingWeapon : BaseWeapon
    {
        [SerializeField] private Transform _spawnPoint;
        const float EffectMaxLength = 2.9f;

        public override void Shoot()
        {
            var shootEffect =
                _shootEffectPool.GetElementWithPositionRotation(_spawnPoint.position, _spawnPoint.rotation);

            _shootEffectPool.PutElementDelayed(shootEffect, EffectMaxLength);

            var bullet = _bulletPrefabPool.GetElementWithPositionRotation(_spawnPoint.position, _spawnPoint.rotation);

            bullet.GetComponent<BaseBullet>()?.SetDirection(_spawnPoint.forward);
        }
    }
}