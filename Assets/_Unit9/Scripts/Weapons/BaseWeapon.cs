﻿// Created by Ragueel

using _Unit9.Scripts.GameSO;
using UnityEngine;

namespace _Unit9.Scripts.Weapons
{
    public abstract class BaseWeapon : MonoBehaviour
    {
        [SerializeField] protected SimpleObjectsPoolSO _shootEffectPool;
        [SerializeField] protected SimpleObjectsPoolSO _bulletPrefabPool;

        public abstract void Shoot();
    }
}