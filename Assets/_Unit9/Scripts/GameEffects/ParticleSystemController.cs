﻿// Created by Ragueel

using UnityEngine;

namespace _Unit9.Scripts.GameEffects
{
    public class ParticleSystemController : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _particleSystem;

        private void OnEnable()
        {
            _particleSystem.Simulate(0.0f, true, true);
            _particleSystem.Play();
        }
    }
}