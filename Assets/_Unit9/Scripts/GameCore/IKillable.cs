﻿namespace _Unit9.Scripts.GameCore
{
    public interface IKillable
    {
        void Die();
    }
}