﻿// Created by Ragueel

namespace _Unit9.Scripts.GameCore
{
    public interface IResettable
    {
        void Reset();
    }
}