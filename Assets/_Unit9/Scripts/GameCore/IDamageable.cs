﻿namespace _Unit9.Scripts.GameCore
{
    public interface IDamageable
    {
        void TakeDamage(float amount);
    }
}