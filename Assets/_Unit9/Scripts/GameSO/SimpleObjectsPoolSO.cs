﻿// Created by Ragueel

using System;
using System.Collections;
using System.Collections.Generic;
using _Unit9.Scripts.GameManagers;
using UnityEngine;

namespace _Unit9.Scripts.GameSO
{
    [CreateAssetMenu(menuName = "SO/Simple Pool")]
    public class SimpleObjectsPoolSO : ScriptableObject, ISimplePool<GameObject>
    {
        [SerializeField] private GameObject _prefab;
        [SerializeField] private int _initialCount = 10;

        private Queue<GameObject> _objectsPool = new Queue<GameObject>();

        [NonSerialized] private PoolState _poolState = PoolState.NotInitialized;

        private bool PoolIsNotInitialised()
        {
            return _poolState != PoolState.NotInitialized;
        }

        public void TryToInitialize()
        {
            if (PoolIsNotInitialised())
            {
                return;
            }

            if (_prefab != null)
            {
                CreatePoolObjects();
            }
            else
            {
                Debug.LogWarning($"Pool doesn't have prefab: {name}");
            }
        }

        public void Clear()
        {
            _objectsPool.Clear();
        }

        private void CreatePoolObjects()
        {
            for (int i = 0; i < _initialCount; i++)
            {
                var createdElement = CreateElement();
                _objectsPool.Enqueue(createdElement);
            }

            _poolState = PoolState.Initialized;
        }

        private GameObject CreateElement()
        {
            GameObject obj = Instantiate(_prefab);
            obj.SetActive(false);
            return obj;
        }

        public GameObject GetElementWithPositionRotation(Vector3 position, Quaternion rotation)
        {
            var element = GetElement();

            element.transform.position = position;
            element.transform.rotation = rotation;

            return element;
        }

        public void PutElementDelayed(GameObject obj, float delayTime)
        {
            GameplayManager.Instance.StartCoroutine(DelayedPutRoutine(obj, delayTime));
        }

        private IEnumerator DelayedPutRoutine(GameObject obj, float delayTime)
        {
            yield return new WaitForSeconds(delayTime);
            PutElement(obj);
        }

        public GameObject GetElement()
        {
            GameObject obj;
            if (_objectsPool.Count > 0)
            {
                obj = _objectsPool.Dequeue();
            }
            else
            {
                obj = CreateElement();
            }

            obj.SetActive(true);

            return obj;
        }

        public void PutElement(GameObject t)
        {
            t.SetActive(false);
            _objectsPool.Enqueue(t);
        }

        private enum PoolState
        {
            NotInitialized,
            Initialized
        }
    }
}