﻿// Created by Ragueel

using UnityEngine;

namespace _Unit9.Scripts.GameSO
{
    [CreateAssetMenu(menuName = "SO/Float", order = 0)]
    public class FloatFieldSO : ScriptableObject
    {
        public static implicit operator float(FloatFieldSO v)
        {
            return v.Value;
        }

        public float Value;
    }
}