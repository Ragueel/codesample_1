﻿// Created by Ragueel

using UnityEngine;

namespace _Unit9.Scripts.GameSO
{
    [CreateAssetMenu(menuName = "SO/Live Data/Float")]
    public class FloatLiveData : LiveData<float>
    {
    }
}