﻿// Created by Ragueel

using _Unit9.Scripts.Enemies;
using UnityEngine;

namespace _Unit9.Scripts.GameSO.EnemyLogic
{
    [CreateAssetMenu(menuName = "SO/Enemy/Move Random", order = 0)]
    public class MoveRandomAction : BaseEnemyAction
    {
        [SerializeField] private float _randomDirectionChooseTime = 1.5f;
        [SerializeField] private float _movementSpeed = 4;

        public override void ActOn(EnemyController enemyController)
        {
            if (enemyController.MoveTime > _randomDirectionChooseTime)
            {
                enemyController.MoveTime = 0;
                enemyController.MovementDirection = Random.insideUnitSphere;
                enemyController.MovementDirection.y = 0;

                if (IsTooFarFromPlayer(enemyController))
                {
                    enemyController.MovementDirection = (Vector3.zero - enemyController.transform.position).normalized;
                }
            }

            enemyController.transform.position += enemyController.MovementDirection * (_movementSpeed * Time.deltaTime);

            enemyController.MoveTime += Time.deltaTime;
        }

        private static bool IsTooFarFromPlayer(EnemyController enemyController)
        {
            // Vector.zero because player stays on world zero coordinates
            return Vector3.Distance(enemyController.transform.position, Vector3.zero) > 16;
        }
    }
}