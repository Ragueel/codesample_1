﻿// Created by Ragueel

using _Unit9.Scripts.Enemies;
using UnityEngine;

namespace _Unit9.Scripts.GameSO.EnemyLogic
{
    [CreateAssetMenu(menuName = "SO/Enemy/Rotate to player", order = 0)]
    public class RotateToPlayerEnemyAction : BaseEnemyAction
    {
        public override void ActOn(EnemyController enemyController)
        {
            // Vector.zero because player stays on world zero coordinates
            enemyController.transform.LookAt(Vector3.zero);
        }
    }
}