﻿// Created by Ragueel

using _Unit9.Scripts.Enemies;
using UnityEngine;

namespace _Unit9.Scripts.GameSO.EnemyLogic
{
    [CreateAssetMenu(menuName = "SO/Enemy/Shoot", order = 0)]
    public class ShootEnemyAction : BaseEnemyAction
    {
        [SerializeField] private float _shootTime = 0.6f;

        public override void ActOn(EnemyController enemyController)
        {
            enemyController.ShootTimer += Time.deltaTime;

            if (enemyController.ShootTimer > _shootTime)
            {
                enemyController.ShootTimer = 0f;
                enemyController.Weapon.Shoot();
            }
        }
    }
}