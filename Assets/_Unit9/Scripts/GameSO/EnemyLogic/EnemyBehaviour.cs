﻿// Created by Ragueel

using _Unit9.Scripts.Enemies;
using UnityEngine;

namespace _Unit9.Scripts.GameSO.EnemyLogic
{
    /// <summary>
    /// Initially I planned to make it as state and later implement state machine for enemies.
    /// But decided to use this simpler version of it.
    /// </summary>
    [CreateAssetMenu(menuName = "SO/Enemy/Behaviour", order = 0)]
    public class EnemyBehaviour : ScriptableObject
    {
        [SerializeField] private BaseEnemyAction[] _actions;

        public void ActOn(EnemyController enemyController)
        {
            for (int i = 0; i < _actions.Length; i++)
            {
                _actions[i].ActOn(enemyController);
            }
        }
    }
}