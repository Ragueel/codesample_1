﻿// Created by Ragueel

using _Unit9.Scripts.Enemies;
using UnityEngine;

namespace _Unit9.Scripts.GameSO.EnemyLogic
{
    public abstract class BaseEnemyAction : ScriptableObject
    {
        public abstract void ActOn(EnemyController enemyController);
    }
}