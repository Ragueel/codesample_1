﻿namespace _Unit9.Scripts.GameSO
{
    public interface ISimplePool<T>
    {
        T GetElement();
        void PutElement(T t);
    }
}