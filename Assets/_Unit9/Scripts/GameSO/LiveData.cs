﻿// Created by Ragueel

using UnityEngine;

namespace _Unit9.Scripts.GameSO
{
    public abstract class LiveData<T> : GameEventSO
    {
        protected T _val;

        public T Value
        {
            get => _val;
            set
            {
                _val = value;
                _event.Invoke();
            }
        }
    }
}