﻿// Created by Ragueel

using System;
using UnityEngine;

namespace _Unit9.Scripts.GameSO
{
    [CreateAssetMenu(menuName = "SO/Events/Empty", order = 0)]
    public class GameEventSO : ScriptableObject
    {
        protected Action _event;

        public void Raise()
        {
            _event?.Invoke();
        }

        public void AddListener(Action listener)
        {
            _event += listener;
        }

        public void RemoveListener(Action listener)
        {
            _event -= listener;
        }
    }
}