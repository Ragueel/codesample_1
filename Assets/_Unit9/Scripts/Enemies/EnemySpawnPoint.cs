﻿// Created by Ragueel

using _Unit9.Scripts.GameSO;
using UnityEngine;

namespace _Unit9.Scripts.Enemies
{
    public class EnemySpawnPoint : MonoBehaviour
    {
        [SerializeField] private SimpleObjectsPoolSO _effectsPool;
        const float EffectLength = 2f;

        public void SpawnEnemy(GameObject enemy)
        {
            enemy.transform.position = transform.position;
            enemy.GetComponent<EnemyHpController>()?.Reset();

            var obj = _effectsPool.GetElementWithPositionRotation(transform.position, transform.rotation);

            _effectsPool.PutElementDelayed(obj, EffectLength);
        }
    }
}