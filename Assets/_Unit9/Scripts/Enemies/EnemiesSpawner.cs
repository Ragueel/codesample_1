﻿// Created by Ragueel

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Unit9.Scripts.GameSO;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Unit9.Scripts.Enemies
{
    public class EnemiesSpawner : MonoBehaviour
    {
        [SerializeField] private SpawnPointHolder[] _enemySpawnPoints;
        [SerializeField] private SimpleObjectsPoolSO _redEnemiesPool;
        [SerializeField] private SimpleObjectsPoolSO _blueEnemiesPool;
        [SerializeField] private FloatLiveData _playerScore;
        [SerializeField] private GameEventSO _onEnemyDeath;
        [SerializeField] private GameEventSO _onWaveComplete;

        private int _currentDifficulty = 1;
        private int _totalEnemySpawnCount = 0;
        private int _deadEnemyCount;

        private void OnEnable()
        {
            _playerScore.AddListener(OnPlayerScoreChange);
            _onEnemyDeath.AddListener(OnEnemyDeath);
        }

        private void OnDisable()
        {
            _playerScore.RemoveListener(OnPlayerScoreChange);
            _onEnemyDeath.RemoveListener(OnEnemyDeath);
        }

        private void OnEnemyDeath()
        {
            _deadEnemyCount++;
            Debug.Log($"Enemies died {_deadEnemyCount} {_totalEnemySpawnCount}");

            if (_deadEnemyCount >= _totalEnemySpawnCount)
            {
                _deadEnemyCount = 0;
                StartCoroutine(SpawnEnemiesDelayedRoutine());
            }
        }

        private IEnumerator SpawnEnemiesDelayedRoutine()
        {
            _onWaveComplete.Raise();
            yield return new WaitForSeconds(2.5f);
            SpawnEnemies();
        }

        private void OnPlayerScoreChange()
        {
            if (_playerScore.Value > 75)
            {
                _currentDifficulty = 2;
            }
        }

        public void SpawnEnemies()
        {
            if (_currentDifficulty >= 2)
            {
                int randomSpawnCount = Random.Range(2, 5);

                SpawnEnemiesOfType(EnemyType.Red, randomSpawnCount);

                int blueRandomSpawnCount = Random.Range(1, 3);

                SpawnEnemiesOfType(EnemyType.Blue, blueRandomSpawnCount);

                _totalEnemySpawnCount = randomSpawnCount + blueRandomSpawnCount;

                Debug.Log($"Enemies spawned: {_totalEnemySpawnCount}");
            }
            else
            {
                int randomSpawnCount = Random.Range(2, 5);

                SpawnEnemiesOfType(EnemyType.Red, randomSpawnCount);
                _totalEnemySpawnCount = randomSpawnCount;
            }
        }

        private List<SpawnPointHolder> TryToSelectRandom(List<SpawnPointHolder> spawnPoints, int count)
        {
            int[] collectedIndices = new int[count];
            List<SpawnPointHolder> randomSpawnPoints = new List<SpawnPointHolder>();

            for (int i = 0; i < count; i++)
            {
                while (true)
                {
                    var randomSpawnPoint = Random.Range(0, spawnPoints.Count);

                    if (!collectedIndices.Contains(randomSpawnPoint))
                    {
                        collectedIndices[i] = randomSpawnPoint;
                        randomSpawnPoints.Add(spawnPoints[randomSpawnPoint]);
                        break;
                    }
                }
            }

            return randomSpawnPoints;
        }

        private SimpleObjectsPoolSO GetPoolForEnemyType(EnemyType enemyType)
        {
            if (enemyType == EnemyType.Red)
            {
                return _redEnemiesPool;
            }

            return _blueEnemiesPool;
        }

        private void SpawnEnemiesOfType(EnemyType enemyType, int count)
        {
            var enemySpawnPoints = _enemySpawnPoints.Where(a => a.EnemyTypes == enemyType).ToList();
            var randomlySelectedSpawnPoints = TryToSelectRandom(enemySpawnPoints, count);

            var pool = GetPoolForEnemyType(enemyType);

            foreach (SpawnPointHolder randomlySelectedSpawnPoint in randomlySelectedSpawnPoints)
            {
                var enemy = pool.GetElement();
                randomlySelectedSpawnPoint.SpawnPoint.SpawnEnemy(enemy);
            }
        }
    }

    [Serializable]
    public class SpawnPointHolder
    {
        [SerializeField] private EnemySpawnPoint _spawnPoint;
        [SerializeField] private EnemyType _enemyType;

        public EnemyType EnemyTypes => _enemyType;
        public EnemySpawnPoint SpawnPoint => _spawnPoint;
    }
}