﻿// Created by Ragueel

using _Unit9.Scripts.GameCore;
using _Unit9.Scripts.GameSO;
using _Unit9.Scripts.GameUI;
using UnityEngine;

namespace _Unit9.Scripts.Enemies
{
    public class EnemyHpController : MonoBehaviour, IDamageable, IKillable, IResettable
    {
        [SerializeField] private FloatFieldSO _maxHp;
        [SerializeField] private SimpleObjectsPoolSO _enemyDeathEffectPool;
        [SerializeField] private SimpleObjectsPoolSO _enemyPool;
        [SerializeField] private GameEventSO _enemyDeathEvent;
        [SerializeField] private ProgressBarController _hpBar;
        const float DeathEffectLength = 5f;
        private float _currentHp;

        private void Awake()
        {
            Reset();
        }

        public void TakeDamage(float amount)
        {
            _currentHp -= amount;
            _hpBar.SetValue(_currentHp / _maxHp);

            if (_currentHp <= 0)
            {
                Die();
            }
        }

        public void Die()
        {
            var effect = _enemyDeathEffectPool.GetElementWithPositionRotation(transform.position, transform.rotation);
            _enemyDeathEffectPool.PutElementDelayed(effect, DeathEffectLength);

            _enemyDeathEvent.Raise();

            _enemyPool.PutElement(gameObject);
        }

        public void Reset()
        {
            _currentHp = _maxHp;
            _hpBar.SetValue(_currentHp / _maxHp);
        }
    }
}