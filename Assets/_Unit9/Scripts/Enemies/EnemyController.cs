﻿// Created by Ragueel

using System;
using _Unit9.Scripts.GameSO.EnemyLogic;
using _Unit9.Scripts.Weapons;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Unit9.Scripts.Enemies
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private BaseWeapon _weapon;
        [SerializeField] private EnemyBehaviour _behaviour;
        [NonSerialized] public float ShootTimer;
        public BaseWeapon Weapon => _weapon;
        public Vector3 MovementDirection;
        public float MoveTime;

        private void Awake()
        {
            ShootTimer = Random.Range(0, 1f);
        }

        private void Update()
        {
            _behaviour.ActOn(this);
        }
    }
}