﻿// Created by Ragueel

using UnityEngine;

namespace _Unit9.Scripts.GameUtils
{
    public class FaceCamera : MonoBehaviour
    {
        [SerializeField] private bool _inverted;

        private Camera _camera;
        Transform _transform;

        private void Awake()
        {
            _transform = transform;
            _camera = Camera.main;
        }

        private void Update()
        {
            if (_inverted)
            {
                _transform.forward = (_transform.position - _camera.transform.position).normalized;
            }
            else
            {
                _transform.forward = (_camera.transform.position - _transform.position).normalized;
            }
        }
    }
}