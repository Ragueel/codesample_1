# Project info

Unity version: ***2019.4.15f1***

# How to play in Editor

After downloading and starting the project in Unity Editor, go to _Unit9/Scenes/MainScene_

Open this Scene.

Press Run button.

# How to build

First of all, you need a cardboard compatible phone and cardboard. The list of phones is here:

[https://mrcardboard.eu/list-of-google-cardboard-compatible-phones-that-work-with-virtual-reality-glasses/](https://mrcardboard.eu/list-of-google-cardboard-compatible-phones-that-work-with-virtual-reality-glasses/)

Then you have to install cardboard:

[https://play.google.com/store/apps/details?id=com.google.samples.apps.cardboarddemo&hl=en_US](https://play.google.com/store/apps/details?id=com.google.samples.apps.cardboarddemo&hl=en_US)


In Unity Editor, switch platform to android.

After switching to android, click build and run.